<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wpbase
 */

?>

	</div><!-- #content -->

	<div class="newsletter">

		<div class="container text-center">

			<div class="row">
				<div class="col-md-12">

					<h1 class="newsletter--title">Email Newsletter</h1>
					<p class="newsletter--para">
						Sign up to receive the lattest news, training tips, healhty recipes, and important updates from our online community.
					</p>

					<div class="newsletter--form">

						<form class="" action="" method="post">

							<div class="form-group">
								<input type="text" name="name" placeholder="First Name" >
							</div>

							<div class="form-group">
								<input type="email" name="name" placeholder="Email Address" >
							</div>

							<div class="form-group">
								<input type="submit" name="submit" value="Subscribe">
							</div>

						</form>

					</div>

				</div>
			</div>

		</div>

	</div><!-- newsletter -->

	<footer class="footer">
		<div class="container clearfix">

			<div class="footer--copy">
				<p>© <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
			</div>

			<div class="footer--nav">
				<?php wp_nav_menu(array('theme_location'=>'social')); ?>
			</div>

		</div>
	</footer><!-- footer -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
