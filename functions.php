<?php
/**
 * wpbase functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package wpbase
 */

if ( ! function_exists( 'wpbase_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wpbase_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on wpbase, use a find and replace
	 * to change 'wpbase' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'wpbase', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Nav', 'wpbase' ),
		'social' => esc_html__( 'Social Nav', 'wpbase' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'wpbase_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	//load needed Plugins
	register_plugins();
}
endif; // wpbase_setup
add_action( 'after_setup_theme', 'wpbase_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wpbase_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'wpbase_content_width', 640 );
}
add_action( 'after_setup_theme', 'wpbase_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
// function wpbase_widgets_init() {
// 	register_sidebar( array(
// 		'name'          => esc_html__( 'Sidebar', 'wpbase' ),
// 		'id'            => 'sidebar-1',
// 		'description'   => '',
// 		'before_widget' => '<section id="%1$s" class="widget %2$s">',
// 		'after_widget'  => '</section>',
// 		'before_title'  => '<h2 class="widget-title">',
// 		'after_title'   => '</h2>',
// 	) );
// }
// add_action( 'widgets_init', 'wpbase_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wpbase_scripts() {

	/** Styles **/
	wp_enqueue_style( 'wpbase-font-awesome', get_template_directory_uri() .'/bower_components/font-awesome/css/font-awesome.min.css' );
	wp_enqueue_style( 'wpbase-animation', 	 get_template_directory_uri() .'/bower_components/animate.css/animate.min.css' );
	wp_enqueue_style( 'wpbase-bootstrap', 	 get_template_directory_uri() .'/bower_components/bootstrap/dist/css/bootstrap.min.css' );
	wp_enqueue_style( 'wpbase-flex', 	 			 get_template_directory_uri() .'/bower_components/flexslider/flexslider.css' );

	wp_enqueue_style( 'wpbase-style', 		 	 get_stylesheet_uri() );
	wp_enqueue_style( 'wpbase-theme', 		 	 get_template_directory_uri() .'/css/styles.css' );

	/** Scripts **/
	wp_enqueue_script( 'wpbase-jquery', 			get_template_directory_uri() . '/bower_components/jquery/dist/jquery.min.js', array(), '20120203', true );
	wp_enqueue_script( 'wpbase-bootstrap', 		get_template_directory_uri() . '/bower_components/bootstrap/dist/js/bootstrap.min.js', array(), '20120205', true );
	wp_enqueue_script( 'wpbase-navigation', 	get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'wpbase-flex', 				get_template_directory_uri() . '/bower_components/flexslider/jquery.flexslider.js', array(), '20120206', true );
	wp_enqueue_script( 'wpbase-scripts', 			get_template_directory_uri() . '/js/app.js', array(), '20120207', true );

	wp_enqueue_script( 'wpbase-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wpbase_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Shortcodes
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * Include Plugins
 */

function register_plugins()
{
	require get_template_directory() . '/inc/TGMPA-TGM/class-tgm-plugin-activation.php';


	$plugins = array(
				array(
					'name'     	=> 'Advanced Custom Fields Pro',
					'slug'     	=> 'acf_pro',
					'source'   	=> get_template_directory() . '/assets/advanced-custom-fields-pro.zip',
					'required' 	=> true
				),

				array(
					'name'     	=> 'Visual Composer',
					'slug'     	=> 'js_composer',
					'source'   	=> get_template_directory() . '/assets/js_composer.zip',
					'required' 	=> true
				),

				array(
					'name'   => 'Jetpack by WordPress',
					'slug'   => 'jetpack',
					'required' => true
				),

				array(
					'name'   => 'Breadcrumb NavXT',
					'slug'   => 'breadcrumb-navxt',
					'required' => false
				)
			);

	tgmpa($plugins, array('is_automatic' => true));

}

/**
 * Theme Options Page Used ACF Options Page
 */

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> true
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));

}
