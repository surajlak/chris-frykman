<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wpbase
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wpbase' ); ?></a>

	<header class="header">
		<div class="container clearfix">

				<div class="header--brnading">
					<a href="<?php bloginfo('url'); ?>"><?php bloginfo('name') ?></a>
				</div>

				<a href="#" class="header--nav-toggle"></a>

				<nav class="header--primary-nav">
					<?php wp_nav_menu(array('theme_location'=>'primary')); ?>
				</nav>
		</div>
	</header><!-- header -->

	<div class="mobile-header">
		<a href="#" class="header--nav-toggle"><i class="fa fa-close"></i></a>

		<?php wp_nav_menu(array('theme_location'=>'primary')); ?>
	</div>

	<div id="content" class="site-content">
