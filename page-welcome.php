<?php
/**
 * Template Name: Frount Page Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wpbase
 */

get_header(); ?>

	<div class="l-main">

		<?php query_posts(array('post_type'=>'slides','post_per_page'=>5)); ?>
		<?php if(have_posts()): ?>
		<div class="hero">

		  <div class="flexslider">
		    <ul class="slides">
		      <?php while(have_posts()): the_post(); ?>

		      <li>
		        <div class="slide text-center" style="background-image:url('<?php the_field('slide_image') ?>')">
		            <!-- <img src="<?php the_field('slide_image'); ?>" class="slide--img img-responsive" alt="" /> -->

		            <div class="slide--content">

		              <?php if(get_field('slide_title')): ?>
		                  <div class="slide--title"><h1><?php the_field('slide_title') ?></h1></div>
		              <?php endif; ?>

		              <?php if(get_field('slide_description')): ?>
		                    <div class="slide--desc"><?php the_field('slide_description') ?></div>
		              <?php endif; ?>


		              <?php if(get_field('button_link') !="" && get_field('button_text') !=""): ?>
		                <a href="<?php the_field('button_link'); ?>" class="btn"><?php the_field('button_text'); ?></a>
		              <?php endif;  ?>

		            </div>
		        </div>
		      </li>

		      <?php endwhile; ?>
		    </ul>
		  </div>

		</div>

		<?php endif; ?>
		<?php wp_reset_query(); ?>

		<div class="container">
			<div class="row">

				<div id="primary" class="content-area col-md-12">
					<main id="main" class="site-main" role="main">

					<?php
					if ( have_posts() ) :

						if ( is_home() && ! is_front_page() ) : ?>
							<header>
								<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
							</header>

						<?php
						endif;

						/* Start the Loop */
						while ( have_posts() ) : the_post();

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'template-parts/content', 'welcome');

						endwhile;

						the_posts_navigation();

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif; ?>

					</main><!-- #main -->
				</div><!-- #primary -->


			</div>
		</div>


	</div>



<?php
get_footer();
