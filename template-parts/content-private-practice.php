<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wpbase
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('private-practice'); ?>>
	<header class="entry-header">

		<div class="entry-header--title text-center">
				<h1><?php the_title(); ?></h1>
		</div>

		<?php $icon = get_field('practice_icon'); ?>
		<?php $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
		<?php if ($thumb!=""): ?>
			<div class="entry-header--img text-center">

				<?php if ($icon!=""): ?>
					<div class="entry-header--icon ">
							<img src="<?php echo $icon ?>" alt="" />
					</div>
				<?php endif; ?>

					<a href="<?php the_permalink(); ?>"><img src="<?php echo $thumb; ?>" alt="" /></a>
			</div>
		<?php endif; ?>

	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php $sub_title = get_field('practice_sub_title'); ?>

		<?php if ($sub_title!=""): ?>
			<div class="entry-content--subtext ">
					<h3 class="simple-title"><?php echo $sub_title; ?></h3>
			</div>
		<?php endif; ?>

		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'wpbase' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wpbase' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<a href="<?php the_permalink(); ?>" class="btn btn-primary">Continue Reading</a>
		<!-- <?php wpbase_entry_footer(); ?> -->
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
